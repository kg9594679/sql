## Задание №6. Создание SQL-запросов

1. **Таблица Customers. Получили данные в каких странах присутствуют клиенты**   
SELECT DISTINCT Country FROM Customers

![sql1](src/pictures/sql1.png) 

2. **Таблица Customers. Получили список клиентов только из Женевы** 

SELECT CustomerID, CustomerName, City FROM Customers WHERE City = "Genève"

![sql2](src/pictures/sql2.png)

3. **Таблица Customers. Получили клиентов только из стран: Швеции, Испании, Швейцарии, отсортированного по городу**

SELECT * FROM Customers WHERE Country IN ('Sweden','Spain','Switzerland') ORDER BY City

![sql3](src/pictures/sql3.png)

4. **Таблица Categories. Получили таблицу категорий товаров, отсортированных по названию категории (CategoryName) в обратном алфавитному порядке** 

SELECT * FROM Categories ORDER by CategoryName DESC

![sql4](src/pictures/sql4.png)      

5. **Таблица Categories. Получили общее число категорий товаров из таблицы Categories**

SELECT COUNT(CategoryID) FROM Categories

![sql5](src/pictures/sql5.png)

6. **Таблица Categories. Получили категории товаров с описанием на букву "Д" и сортировкой по столбцу названия категории товаров в алфавитном порядке** 
SELECT * FROM Categories WHERE Description LIKE "D%" ORDER BY CategoryName

![sql6](src/pictures/sql6.png)


7. **Таблица Employees. Получили список сотрудников, отсортированных по фамилии в алфавитном порядке**   

SELECT EmployeeID,LastName, FirstName  FROM Employees ORDER BY LastName    

![sql7](src/pictures/sql7.png)   

8. **Таблица Employees. Получили спискок сотрудников моложе 1965 года рождения, отсортированного по возрастанию  имени**

SELECT *     FROM Employees   WHERE BirthDate > '1965%'   ORDER BY FirstName

![sql8](src/pictures/sql8.png)

9. **Таблица Employees. Получили список сотрудников, у которых степень бакалавра по английскому языку и отсортировать по имени в алфавитном порядке**

SELECT EmployeeID, LastName, FirstName, Notes FROM Employees WHERE Notes LIKE '%BA degree in English%' ORDER BY FirstName

![sql9](src/pictures/sql9.png) 

10. **Таблица OrderDetails. Получили список детализации заказа, где ID товара равен 36**  

SELECT * FROM OrderDetails WHERE ProductID = 36

![sql10](src/pictures/sql10.png)

11. **Таблица OrderDetails. Получили количество всех заказанных товаров**

SELECT SUM(Quantity) FROM OrderDetails

![sql11](src/pictures/sql11.png)

12. **Таблица OrderDetails. Получили список детализаций заказов, где количество товаров в заказе суммарно больше 150, отсортированного по возрастанию ID**     

SELECT OrderID, sum(Quantity) AS Qu   FROM OrderDetails   GROUP BY OrderID   HAVING Qu > 150   ORDER BY OrderID;  

![sql12](src/pictures/sql12.png)

13. **Таблица Orders. Получили список заказов, где ID клиента = 80**  

SELECT *    FROM Orders   WHERE CustomerID=80

![sql13](src/pictures/sql13.png)

14. **Таблица Orders. Получили все заказы, сделанные покупателем под номером 62 и отправленные отправителем под номером 1**

SELECT * FROM Orders WHERE CustomerID = 62 AND ShipperID = 1

![sql14](src/pictures/sql14.png)

15. **Таблица Orders. Получили список заказов после 1997-02-07, отсортированного по возрастанию даты**  

SELECT *    FROM Orders   WHERE OrderDate >= "1997-02-07"   ORDER BY OrderDate  

![sql15](src/pictures/sql15.png)

16. **Таблица Products. Получили список товаров в "банках", отсортированного по возрастанию цены** 

SELECT *    FROM Products   WHERE Unit LIKE '%cans'   ORDER BY Price  

![sql16](src/pictures/sql16.png)

17. **Таблица Products. Получили наименования продуктов 1 категории и поставщиков этих продуктов, упорядочив по ID поставщика**

SELECT ProductName,SupplierID,CategoryID FROM Products where CategoryID=1 order by SupplierID

![sql17](src/pictures/sql17.png)

18. **Таблица Products. Получили наименования всех продуктов, у которых цена в диапазоне от 9,65 до 12,5, поставщиков и категорию, упорядочить по категории**

SELECT ProductName,SupplierID,CategoryID,Price FROM Products where Price>=9.65 and Price<=12.5 order by CategoryID

![sql18](src/pictures/sql18.png)


19. **Таблица Shippers. Получили список грузоотправителей, отсортированных по номеру телефона в обратном алфавитном порядке**      

SELECT * FROM Shippers order by Phone desc

![sql19](src/pictures/sql19.png)

20.  **Таблица Shippers. Получили список грузоперевеозчиков, где в имени отправителя есть фраза "United"**   

SELECT *    FROM Shippers   WHERE ShipperName LIKE '%United%'

![sql20](src/pictures/sql20.png)

21. **Таблица Shippers. Получили список грузоотправителей, в номере телефона которых есть "99", отсортированного по имени отправителя**  

SELECT *    FROM Shippers   WHERE Phone LIKE '%99%'   ORDER BY ShipperName 

![sql21](src/pictures/sql21.png)

22. **Таблица Suppliers. Получили список поставщиков из Сингапура**   

SELECT *    FROM Suppliers   WHERE Country = 'Singapore'

![sql22](src/pictures/sql22.png)

23. **Таблица Suppliers. Получили поставщиков из городов Oviedo или Cuxhaven, отсортировать по стране**

SELECT * FROM Suppliers  where City = 'Oviedo' or  City = 'Cuxhaven' order by Country

![sql23](src/pictures/sql23.png)

24. **Таблица Suppliers. Получили данные по поставщику "Specialty Biscuits, Ltd", о продуктах, в какой город и по какому адресу он отправил, соединив данные из Suppliers и Products** 

SELECT Suppliers.SupplierName,Suppliers.City,Suppliers.Address, Products.Productname  from Suppliers inner JOIN Products ON Products.SupplierID = Suppliers.SupplierID where Suppliers.SupplierName='Specialty Biscuits, Ltd.'

![sql24](src/pictures/sql24.png)

